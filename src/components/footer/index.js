import React from 'react'
import {RowText,Divh1,Rowh1,RedContainer,Img} from './foterElements'

const FooterRed = () => {
    return (
        <>
        <RedContainer>
            <RowText>
                <Divh1>
                    <Rowh1>Copyrights © 2020 Manoo - Todos os direitos reservados 
                    </Rowh1>
                    <Rowh1>Termos de Uso | Políticas de Privacidade </Rowh1>
                </Divh1>
            </RowText>
        </RedContainer>
        </>
    )
}

export default FooterRed
