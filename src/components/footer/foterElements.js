import styled from 'styled-components';


export const RedContainer = styled.section `
    
background: #343a40;
height: 60px;

@media screen and (max-width: 767px) {
    height: 250px;
    img {
    width: 79%;
    }
}

`

export const RowText = styled.div `
max-width: 1000px;
margin: 0 auto;
display:grid;
grid-template-columns: 1fr  ;
align-items: center;
grid-gap 16px;
padding: 0 0px;
`
export const Divh1 = styled.div `
    padding-left: 46px;
    margin-top: 25px;
    
    


    p {
        font-size: 1.5rem;
        color:#343a40;
    }
`
export const Rowh1 = styled.h1 `
    color: #fff;
    font-size: 14px;
    text-align: center;
`
export const Img = styled.img `
width: 30%;
`