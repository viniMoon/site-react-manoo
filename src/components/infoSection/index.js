import React from 'react'

import {InfoContainer,infoWrapper,InfoRow,Column1,TextWrapper,
    TopLine,Heading,Subtitle,BtnWrap,Column2,
    ImgWrap,Img,Button} from './infoElements'

import mobileImg from '../../images/mobiledemo.png'

const InfoSection = (img,alt,buttonLabel) => {
    return (
        <>
            <InfoContainer id="revenda">
                <infoWrapper id="topo">   
                    <InfoRow>
                        <Column1>
                            <TextWrapper>   
                                <Heading>Ofereça  <b>aos seus clientes</b>  uma solução <b>segura e moderna.</b> </Heading>
                                <BtnWrap>
                                    <Button to="home">SOLICITE AGORA </Button>
                                </BtnWrap>
                            </TextWrapper>
                        </Column1>
                        <Column2>
                            <ImgWrap>
                                <Img src={mobileImg} alt={alt}/>
                            </ImgWrap>
                        </Column2>
                    </InfoRow>
                </infoWrapper>
            </InfoContainer>
        </>
    )   
}

export default InfoSection
