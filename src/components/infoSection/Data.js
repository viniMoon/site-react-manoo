export const homeObject =  {
    id: 'about',
    lightBg:false,
    Lightext:true,
    LightTextDesc: true,
    topLine: 'xablau',
    headLine: 'Ofereça aos seus clientes uma solução segura e moderna.',
    description: 'get acess',
    buttonLabel: 'SOLICITE AGORA',
    imgStart: false,
    img: require('../../images/mobiledemo.png'),
    alt: 'App mobile',
    dark: true,
    primary: true,
    darkText: false
}