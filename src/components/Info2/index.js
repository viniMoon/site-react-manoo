import React from 'react'

import {InfoContainer,infoWrapper,InfoRow,Column1,TextWrapper,
    TopLine,Heading,Subtitle,BtnWrap,Column2,
    ImgWrap,Img,Button,UlItems} from './Info2Elements'

import Restaurante from '../../images/modelo-zapfacil-1.png'

const InfoSection = (img,alt,buttonLabel) => {
    return (
        <>
            <InfoContainer id="vantagens" >
                <infoWrapper>   
                    <InfoRow>
                        <Column1>
                            <TextWrapper>   
                                <Heading>Quando seu restaurante reabrir,
                                      <b>você vai usar cardápio de papel?</b>     
                                </Heading>
                                <UlItems>
                                        <ul>
                                            <li>Acesso via QR Code disponível na mesa;</li>
                                            <li>Acesso por link. Acesse de onde estiver;</li>
                                            <li>Fácil de usar e 99,9% online;</li>
                                            <li>Ambiente seguro e instalado nas nuvens;</li>
                                            <li>Design customizado. Altere quando quiser;</li>
                                            <li>Sistema de avaliação para o cliente;</li>
                                            <li>Sem mensalidades. Pagamento único;</li>
                                            <li>Acesso vitalício com atualizações grátis;</li>
                                            <li>Garantia de 7 dias ou o dinheiro de volta;</li>
                                            <li>Alterações grátis por 3 meses + treinamento;</li>
                                            <li>Suporte local ou através do chat e Whatsapp.</li>
                                        </ul>   
                                </UlItems>
                                <BtnWrap>
                                    <Button to="home">SOLICITE AGORA </Button>
                                </BtnWrap>
                            </TextWrapper>
                        </Column1>
                        <Column2>
                            <ImgWrap>
                                <Img src={Restaurante} alt={alt}/>
                            </ImgWrap>
                        </Column2>
                    </InfoRow>
                </infoWrapper>
            </InfoContainer>
        </>
    )   
}

export default InfoSection
