import styled from 'styled-components';

export const InfoContainer = styled.div `
    color: #fff;
    background-position-x: center;
    background-position-y: top;
    background-color: #f7f8fc;
    background-size: cover;
    

    @media screen and (max-width: 768px) {
        padding: 100px 0;
    }
`


export const infoWrapper = styled.div `
   display:grid;
   z-index:1;
   height: 860px;
   width: 100%;
   max-width: 1100px;
   margin-right: auto;
   margin-left: auto;
   padding: 0 24px;
   justify-content:center;
  
`


export const InfoRow = styled.div `
    
   display:grid;
   grid-auto-columns: minmax(auto, 1fr);
   align-items:center;
   grid-template-areas: ${({ImgStart}) => (ImgStart ? `'col2 col1'` :  `'col1 col2'`)};

   @media screen and (max-width: 768px) {
    grid-template-areas: ${({ImgStart}) => (ImgStart ? `'col1' 'col2'` :  `'col1 col1' 'col2 col2' `)};
   }
`


export const Column1 = styled.div `
margin-top: 30px;
   margin-bottom: 15px;
   padding: 0 15px;
   grid-area: col1;
   margin-left: 80px;

   @media screen and (max-width:767px) {
      margin-left: 0;
   }
`
export const Column2 = styled.div `
   margin-top: 100px;
   margin-bottom: 15px;
   padding: 0 15px;
   grid-area: col2;

   @media screen and (max-width:767px) {
      margin-left: 0;
      margin-top: 0px;
   } 
   
`
export const TextWrapper = styled.div `
  max-width:540px;
  padding-top: 0;
  padding-bottom: 60px;
  
`
export const TopLine = styled.p `
  color: #000;
  font-size: 16px;
  line-height: 16px;
  font-weight: 700;
  letter-spacing: 1.5px
  margin-bottom: 16px;
  
`
export const Heading = styled.h1 `
 margin-bottom: 24px;
 font-size:48px;
 font-weight: 600;
 color: #000;
 text-align: center;
 font-size: 2.3rem;
 line-height: 1.4;
 font-family: 'Roboto Medium'  !important;
 font-weight: 100;

 @media screen and (max-width: 480px) {
     font-size:32px;
 }
  
`
export const UlItems = styled.ul `
    font-size: 20px;
    display: flex;
    flex-direction: column;
    line-height: 1.2;
    text-align: left;
    font-weight: 500;
    margin-left: 60px;
    margin-bottom: 16px;
    color: #000;
    list-style: disc;
  
`
export const Subtitle = styled.p `
 max-width:440px;
 margin-bottom:35px;
 font-size: 18px;
 line-height: 24px;
 color: #000;

`
export const BtnWrap = styled.div `
display:flex;
justify-content:flex-start;
`
export const ImgWrap = styled.div `
margin-bottom: 130px;
 max-width: 555px;
 height: 100%;
`
export const Img = styled.img `
 width: 100%;
 margin: 0 0 10px 0;
 padding-right: 0;

 @media screen and (max-width: 767px) {
   width: 100%;
 }
`
export const Button = styled.a `
color: #fff;
background-color: #dc3545;
border-color: #dc3545;
width: 100%;
padding: 20px;
border-radius: 6px;
text-align: center;
cursor:pointer;


&:hover{
    box-shadow: 3px 4px 29px 8px #dc3545;
    cursor: pointer;
}
`

