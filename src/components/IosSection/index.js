import React from 'react'

import {InfoContainer,Column1,Column2,Img,Rowh1,Rowp,H1Row,RowItems,ItemImg,RowText,Divh1,RowT} from './IosElements'

import mobileImg from '../../images/mobiledemo.png'
import Ios from '../../images/logo_apple.png'
import Android from '../../images/logo_android.png'

const IosSection = (img,alt,buttonLabel) => {
    return (
        <>
            
            <InfoContainer >
            <RowText>
                <Divh1>
                    <RowT>Recursos</RowT>
                    <p>Conheça algumas funcionalidades do Manoo</p>
                </Divh1>
            </RowText>
                <RowItems>  
                    <Column1>
                        <ItemImg>
                            <Img src={Ios} />
                            <h2>No iphone</h2>
                            <ol>
                                <li>Vá em “Ajustes” e acesse as opções da câmera;</li>
                                <li>Ative a função “Escanear Códigos QR”;</li>
                                <li>Abra a câmera do celular e aponte para o código que deseja ler;</li>
                                <li>Vai aparecer um banner com a opção de abrir o código no Safari.</li>
                                <li>Em seguida abrirá o cardápio de forma simples e fácil</li>
                            </ol>
                        </ItemImg>
                    </Column1>
                    <Column2>
                        <ItemImg>
                            <Img src={Android} />
                            <h2>No Android</h2>
                            <ol>
                                <li>Abra o app Câmera integrado. ou <a href="https://bityli.com/sM9ul" class="link-blue text-primary" target="_blank"> Clique aqui</a></li>
                                <li>Aponte a câmera para o código QR</li>
                                <li>Toque no banner que aparece no smartphone ou tablet Android.</li>
                                <li>Siga as instruções na tela para concluir o login.</li>
                                <li>Em seguida abrirá o cardápio de forma simples e fácil</li>
                            </ol>
                        </ItemImg>
                    </Column2>
                </RowItems>
            </InfoContainer>
        </>
    )   
}

export default IosSection
