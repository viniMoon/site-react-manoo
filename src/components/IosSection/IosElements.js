import styled from 'styled-components';
import bgsec from '../../images/waves-header-min.png'

export const InfoContainer = styled.div `
    background-image: url();
    color: #fff;
    background-position-x: center;
    background-position-y: top;
    background-color: #f7f8fc;
    background-size: cover;
    margin-top: -292px;
    background: none;

    @media screen and (max-width: 768px) {
        padding: 100px 0;
    }
`


export const infoWrapper = styled.div `
   display:grid;
   z-index:1;
   height: 860px;
   width: 100%;
   max-width: 1100px;
   margin-right: auto;
   margin-left: auto;
   padding: 0 24px;
   justify-content:center;
`


export const InfoRow = styled.div `
   
   display:grid;
   grid-auto-columns: minmax(auto, 1fr);
   align-items:center;
   grid-template-areas: ${({ImgStart}) => (ImgStart ? `'col2 col1'` :  `'col1 col2'`)};

   @media screen and (max-width: 768px) {
    grid-template-areas: ${({ImgStart}) => (ImgStart ? `'col1' 'col2'` :  `'col1 col1' 'col2 col2' `)};
   }
`
export const Rowh1 = styled.div `
   text-align: left;
   margin-left: 198px;
   margin-bottom: 14px;
   grid-template-areas: ${({ImgStart}) => (ImgStart ? `'col2 col1'` :  `'col1 col2'`)};

   @media screen and (max-width: 768px) {
    grid-template-areas: ${({ImgStart}) => (ImgStart ? `'col1' 'col2'` :  `'col1 col1' 'col2 col2' `)};
    padding-left: 12px;
    margin-left: 0px;

    h1 {
       font-size:2.0rem;
    }

    
   }

   `
export const RowItems = styled.div `
   display:flex;
   justify-content:center;
   alig-items:center;
   flex-wrap:wrap;

   @media screen and (max-width: 768px) {
   grid-template-areas: ${({ImgStart}) => (ImgStart ? `'col1' 'col2'` :  `'col1 col1' 'col2 col2' `)};
   }
   `
export const H1Row = styled.h1 `
    font-size: 2.5rem;
    font-weight:bold;
    color: #000;

    @media screen and (max-width: 767px) {
        font-size: font-size: 2.0rem;;
    }
   
`
export const Rowp = styled.p `
    font-size: 1.3rem;
    margin-top: 6px;
    margin-left: 10px;
    font-family: 'Roboto Thin' !important;
    color: #000;
`
export const ItemImg = styled.div `

    h2 {
       color:#000;
       font-size: 2.5rem;
       font-family: 'Roboto Thin' !important;
       font-weight: 100;
    }

    ol {
      color: #000;
      font-family: 'Roboto Thin' !important;
      padding: 20px;
      font-size: 0.8rem;
      font-weight: 600;
      line-height: 20.9px;
  
    }
    
`


export const Column1 = styled.div `
   margin-bottom: 15px;
   padding: 0 15px;
   grid-area: col1;
   margin-left: 153px;

   @media screen and (max-width:767px) {
      margin-left: 0;
   }
`
export const Column2 = styled.div `
   margin-bottom: 15px;
   padding: 0 15px;
   grid-area: col2;

   @media screen and (max-width:767px) {
      margin-left: 0;
      margin-top: 0px;
   } 
   
`
export const TextWrapper = styled.div `
  max-width:540px;
  padding-top: 0;
  padding-bottom: 60px;
  
`
export const TopLine = styled.p `
  color: #000;
  font-size: 16px;
  line-height: 16px;
  font-weight: 700;
  letter-spacing: 1.5px
  margin-bottom: 16px;
  
`
export const Heading = styled.h1 `
 margin-bottom: 24px;
 font-size:48px;
 font-weight: 600;
 color: #000;
 text-align: center;
 font-size: 2.2rem;
 line-height: 1.4;
 font-family: 'Roboto Medium'  !important;
 font-weight: 100;

 @media screen and (max-width: 480px) {
     font-size:32px;
 }
  
`
export const Subtitle = styled.p `
 max-width:440px;
 margin-bottom:35px;
 font-size: 18px;
 line-height: 24px;
 color: #000;

`

export const ImgWrap = styled.div `
 max-width: 555px;
 height: 100%;
`
export const Img = styled.img `
 width: 20%;
 margin: 0 0 10px 0;
 padding-right: 0;

 @media screen and (max-width: 767px) {
   width: 30%;
 }
`
export const Button = styled.a `
color: #fff;
background-color: #dc3545;
border-color: #dc3545;
width: 100%;
padding: 20px;
border-radius: 6px;
text-align: center;
`

export const RowText = styled.div `
max-width: 1000px;
margin: 0 auto;
display:grid;
grid-template-columns: 1fr  ;
align-items: center;
grid-gap 16px;
padding: 0 0px;
`
export const Divh1 = styled.div `
    padding-left: 46px;
    margin-bottom: 5px;

    p {
        font-size: 1.5rem;
        color: #000;
    }

    @media screen and (max-width:767px) {
       padding-left: 10px;
    }
`
export const RowT = styled.h1 `
  color:#000;
  font-size: 2.5rem;

  p {
     color: #000;
  }
`
