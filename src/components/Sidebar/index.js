import React from 'react'
import {SidebarContainer, 
    Icon, 
    CloseIcon,
    SidebarWrapper,
    SidebarMenu,
    SidebarLink,
    SidebarLinkStrong} from './SidebarElements'

function index({isOpen, toggle}) {
    return (
        <SidebarContainer isOpen={isOpen} onClick={toggle}>
            <Icon onClick={toggle}>
                <CloseIcon />
            </Icon>
            <SidebarWrapper>
                <SidebarMenu>   
                    <SidebarLink to='vantagens'>
                        Vantagens
                    </SidebarLink>
                    <SidebarLink to='revenda'>
                        Revenda
                    </SidebarLink>
                    <SidebarLinkStrong to='pedir'>
                        Peça o seu agora
                    </SidebarLinkStrong>
                </SidebarMenu>
            </SidebarWrapper>
        </SidebarContainer>
    )
}

export default index
