import React from 'react'
import {RowText,Divh1,Rowh1,RedContainer,Img} from './foterredElements'
import logo from '../../images/brand_white.png'

const FooterRed = () => {
    return (
        <>
        <RedContainer>
            <RowText>
                <Divh1>
                    <Rowh1>
                         <Img src={logo} />
                    </Rowh1>
                </Divh1>
            </RowText>
        </RedContainer>
        </>
    )
}

export default FooterRed
