import styled from 'styled-components';


export const RedContainer = styled.section `
    
background: #dc3545;
height: 300px;
padding-top: 71px;
margin-top: 12px;

@media screen and (max-width: 767px) {
    height: 250px;
    img {
    width: 79%;
    }
}

`

export const RowText = styled.div `
max-width: 1000px;
margin: 0 auto;
display:grid;
grid-template-columns: 1fr  ;
align-items: center;
grid-gap 16px;
padding: 0 0px;
`
export const Divh1 = styled.div `
    padding-left: 46px;
    margin-top: 25px;

    p {
        font-size: 1.5rem;
        color:#343a40;
    }
`
export const Rowh1 = styled.h1 `
  color:#343a40;
  font-size: 2.5rem;
`
export const Img = styled.img `
width: 30%;
`