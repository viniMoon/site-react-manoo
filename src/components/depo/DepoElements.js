import styled from 'styled-components';

export const ServicesContainer = styled.div `
    height: 400px;
    display:flex;
    flex-direction:column;
    justify-content:center;
    align-items:center;
    background: #f7f8fc;

    @media screen and (max-width: 768px) {
        height: 1100px;
        margin-top: 20px;
    }
    @media screen and (max-width: 767px) {
        height: 1100px;
        margin-top: 20px;
    }

    @media screen and (max-width: 480px) {
        height: 1064px;
    }

`

export const TextContainer = styled.div `
    display:flex;
`
export const Servicesp = styled.p `
    
`


export const ServicesWrapper = styled.div `
    max-width: 1000px;
    margin: 0 auto;
    display:grid;
    grid-template-columns: 1fr 1fr 1fr ;
    align-items: center;
    grid-gap 16px;
    padding: 0 50px;

    @media screen and (max-width: 1000px) {
        grid-template-columns: 1fr 1fr;
    }

    @media screen and (max-width:768px) {
        grid-template-columns: 1fr;
        padding: 0 20px;
    }
`


export const ServicesCard = styled.div `
   background:#ffffff;
   display: flex;
   flex-direction: column;
   justify-content: flex-start;
   align-items:center;
   border-radius: 10px;
   max-height: 340px;
   padding: 30px;
   box-shadow: 2px 6px 25px 1px #d8d8d8;
   transition: all 0.2s ease-in-out;

   &:hover {
       transform: scale(1.02);
   }
   
`

export const ServicesIcon = styled.img `
  height: 150px;
  width: 150px;
  margin-bottom: 10px;
  border-radius: 50%;
  margin-top: 14px;

   
`

export const ServicesH1 = styled.h1 `
   font-size:2.5rem;
   color: #000;
   margin-bottom: 64px;
   

   @media screen and (max-width: 480px) {
       font-size: 2rem;
   }
`

export const ServicesH2 = styled.h2 `
   font-size:1rem;
   color: #000;
   margin-bottom: 10px;
 
`
export const ServicesP = styled.p `
   font-size: 1rem;
   tex-align:center;
`
export const RowText = styled.div `
max-width: 1000px;
margin: 0 auto;
display:grid;
grid-template-columns: 1fr  ;
align-items: center;
grid-gap 16px;
padding: 0 0px;
`
export const Divh1 = styled.div `
    padding-left: 46px;
    margin-top: 25px;

    p {
        font-size: 1.5rem;
    }
`
export const Rowh1 = styled.h1 `
  color:#000;
  font-size: 2.5rem;
`

