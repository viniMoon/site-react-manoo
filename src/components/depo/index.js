import React from 'react'
import {ServicesContainer,
    ServicesH1,
    ServicesWrapper,
    ServicesCard,
    ServicesIcon,
    ServicesH2,
    ServicesP,
    TextContainer,
    Servicesp,
    RowText,
    Rowh1,
    Divh1,} from './DepoElements'

import trevo from '../../images/trevo_testemunhais.png'    
import trem from '../../images/tremdeminas_testemunhais.png'    
import santa from '../../images/santapizza_testemunhais.png'      

const Depo = () => {
    return (
        <> 
        <RowText>
            <Divh1>
                <Rowh1>O que os nossos clientes estão dizendo</Rowh1>
                <p>Cases de Sucesso</p>
            </Divh1>
        </RowText>
        <ServicesContainer id="services">
            <ServicesWrapper>  
                <ServicesCard>
                    <ServicesP> Essa pandemia nos fez repensar e entre vários protocolos de seguranças, adotamos o cardápio digital que tem sido bem aceito entre nossos clientes.</ServicesP>
                    <ServicesIcon src={trevo} />
                    <ServicesP>Jailton 
                        Gerente Administrativo
                    </ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesP> Foi um grande desafio e tem ajudado muito no nosso dia a dia e a praticidade do cliente acessar aumentou nosso ticket de vendas online</ServicesP>
                    <ServicesIcon src={trem} />
                    <ServicesP>Ivana Gerente
                    </ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesP>Tecnologia sempre fez parta da Santa Pizza. Com esse cardápio ofercemos conforto e segurança ao nosso cliente. Sem falar na economia de cardápio de papel</ServicesP>
                    <ServicesIcon src={santa} />
                    <ServicesP>Fernando Jr.
Direitor Executivo
                    </ServicesP>
                </ServicesCard>
            </ServicesWrapper>
        </ServicesContainer>
        </>
    )
}

export default Depo
