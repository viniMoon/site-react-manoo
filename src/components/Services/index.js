import React from 'react'
import {ServicesContainer,
    ServicesH1,
    ServicesWrapper,
    ServicesCard,
    ServicesIcon,
    ServicesH2,
    ServicesP,
    RowText,
    Divh1,
    Rowh1} from './ServicesElements'

import icon1 from '../../images/icon1.png'    
import icon2 from '../../images/icon2.png'    
import icon3 from '../../images/icon3.png'    
import icon4 from '../../images/icon4.png'    
import icon5 from '../../images/icon5.png'    
import icon6 from '../../images/icon6.png'    
import icon7 from '../../images/icon7.png'    
import icon8 from '../../images/icon8.png'    

const Services = () => {
    return (
        <> 
        <RowText>
            <Divh1>
                <Rowh1>Recursos</Rowh1>
                <p>Conheça algumas funcionalidades do Manoo</p>
            </Divh1>
        </RowText>
        <ServicesContainer id="services">
            <ServicesWrapper>  
                <ServicesCard>
                    <ServicesIcon src={icon1} />
                    <ServicesH2> Por Preços</ServicesH2>
                    <ServicesP> Pesquise pelo preço desejado e receba sugestão em ordem crescente</ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={icon2} />
                    <ServicesH2> Por Preços</ServicesH2>
                    <ServicesP> Pesquise pelo preço desejado e receba sugestão em ordem crescente</ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={icon3} />
                    <ServicesH2> Por Preços</ServicesH2>
                    <ServicesP> Pesquise pelo preço desejado e receba sugestão em ordem crescente</ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={icon4} />
                    <ServicesH2> Por Preços</ServicesH2>
                    <ServicesP> Pesquise pelo preço desejado e receba sugestão em ordem crescente</ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={icon5} />
                    <ServicesH2> Por Preços</ServicesH2>
                    <ServicesP> Pesquise pelo preço desejado e receba sugestão em ordem crescente</ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={icon6} />
                    <ServicesH2> Por Preços</ServicesH2>
                    <ServicesP> Pesquise pelo preço desejado e receba sugestão em ordem crescente</ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={icon7} />
                    <ServicesH2> Por Preços</ServicesH2>
                    <ServicesP> Pesquise pelo preço desejado e receba sugestão em ordem crescente</ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={icon8} />
                    <ServicesH2> Por Preços</ServicesH2>
                    <ServicesP> Pesquise pelo preço desejado e receba sugestão em ordem crescente</ServicesP>
                </ServicesCard>
            </ServicesWrapper>
        </ServicesContainer>
        </>
    )
}

export default Services
