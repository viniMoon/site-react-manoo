import React from 'react'
import {ServicesContainer,
    ServicesH1,
    ServicesWrapper,
    ServicesCard,
    ServicesIcon,
    ServicesH2,
    ServicesP,
    TextContainer,
    Servicesp,
    RowText,
    Rowh1,
    Divh1,
    Servicesh1,
    Servicesz,
    Servicesparcela,
    Btnborder,
    Btnborderred,
    Rowduvida,
    Duvidadiv,
    Duvidah1,
    Duvidah4} from './DepoElements'

import trevo from '../../images/trevo_testemunhais.png'    
import trem from '../../images/tremdeminas_testemunhais.png'    
import santa from '../../images/santapizza_testemunhais.png'      

const Depo = () => {
    return (
        <> 
        <RowText id="pedir">
            <Divh1>
                <Rowh1>O que os nossos clientes estão dizendo</Rowh1>
                <p>Cases de Sucesso</p>
            </Divh1>
        </RowText>
        <ServicesContainer >
            <ServicesWrapper>  
                <ServicesCard className="">
                    <Servicesh1>Anual</Servicesh1>
                    <ServicesP>Garanta resultados por 12 meses</ServicesP>
                    <Servicesz>Zero Mensalidade</Servicesz>
                    <Servicesh1><b className="rsmoney">R$</b> 297</Servicesh1>
                    <Servicesparcela> ou 6x de R$ 49,50</Servicesparcela>
                    <Btnborderred> Quero esse →</Btnborderred>
                </ServicesCard>
                <ServicesCard className="red-bg">
                    <Servicesh1>Vitalício</Servicesh1>
                    <ServicesP>Pague uma vez e use para sempre </ServicesP>
                    <Servicesz>Zero Mensalidade</Servicesz>
                    <Servicesh1><b className="rsmoney">R$</b> 397</Servicesh1>
                    <Servicesparcela> ou 6x de R$ 66,16</Servicesparcela>
                    <Btnborder> Recomendado →</Btnborder>
                </ServicesCard>
            </ServicesWrapper>
        </ServicesContainer>
        </>
    )
}

export default Depo
