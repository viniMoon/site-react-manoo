import React from 'react'
import {CtaContainer,CtaWrapper,CtaBox,SaibaBtn,CtaH1} from './Cta2Elements'

const Cta1 = () => {
    return (
        <CtaContainer>
            <CtaWrapper>
                <CtaBox>
                    <CtaH1><b>Manoô seu cardápio ágil e eficiente</b> </CtaH1>
                </CtaBox>
                <CtaBox>
                    <SaibaBtn> 
                        Solicite uma demonstração agora
                    </SaibaBtn>
                </CtaBox>
            </CtaWrapper>
        </CtaContainer>
    )
}

export default Cta1
