import React from 'react'

import {InfoContainer,infoWrapper,InfoRow,Column1,TextWrapper,
    TopLine,Heading,Subtitle,BtnWrap,Column2,
    ImgWrap,Img,Button,UlItems,Rowduvida,Duvidadiv,Duvidah1,Duvidah4} from './Info3Elements'

import selo from '../../images/warranty-seal-2.png'

const InfoSection = (img,alt,buttonLabel) => {
    return (
        <>
            <InfoContainer >
                <infoWrapper>   
                    <InfoRow>
                        <Column1>
                        <TextWrapper>   
                                <Heading>
                                      <b>SATISFAÇÃO GARANTIDA OU SEU DINHEIRO DE VOLTA</b>     
                                </Heading>
                                <UlItems>
                                    <p>Nós temos certeza que você também vai gostar e por isso oferecemos garantia total de satisfação ou seu dinheiro de volta. Até 7 dias depois do pagamento, você pode solicitar o cancelamento por qualquer motivo e devolvemos todo valor investido.</p> 
                                </UlItems>
                            </TextWrapper>
                        </Column1>
                        <Column2>
                            <ImgWrap>
                                <Img src={selo} alt={alt}/>
                            </ImgWrap>
                        </Column2>
                    </InfoRow>
                </infoWrapper>
            </InfoContainer>
            <Rowduvida>
            <Duvidadiv>
                <Duvidah1>Ficou com alguma dúvida? Fale Conosco!</Duvidah1>
                <Duvidah4>Entre em contato pelo e-mail customer@manoo.com.br ou pelo telefone: (79) 98123-6927 ou clique nos botões abaixo:</Duvidah4>
            </Duvidadiv>
        </Rowduvida>
        </>
    )   
}

export default InfoSection
