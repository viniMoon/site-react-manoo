import React, { useEffect, useState } from 'react'
import {FaBars} from 'react-icons/fa'
import {animateScroll as scroll} from 'react-scroll'

import {Nav,
        NavbarContainer,
        NavLogo,
        MobileIcon,
        NavMenu,
        NavLinks,
        NavItem,
        NavBtn,
        NavBtnLink} from './NavbarElements'

import ImageLogo from './brand.png'


const Navbar = ({toggle}) => {
    const [scrollNav, setScrollNav] = useState(false);

    const changeNav = () => {
        if (window.scrollY >= 80) {
            setScrollNav(true);
        }else {
            setScrollNav(false)
        }
    };

    useEffect(() => {
        window.addEventListener('scroll', changeNav);
    }, [])

    const toggleHome = () => {
        scroll.scrollToTop();
    }

    const [src, setScroll] = useState(false)
    useEffect(() => {
      window.addEventListener("src", () => {
        setScroll(window.scrollY > 10)
      })
    }, [])
    return (
       <>
         <Nav className={src ? "bg-white" : "bg-none"}>
             <NavbarContainer>
                 <NavLogo to='/' onClick={toggleHome}>
                     <img  src={ImageLogo} />
                </NavLogo>
                 <MobileIcon onClick={toggle}>
                     <FaBars />
                 </MobileIcon>
                 <NavMenu>  
                     <NavItem>  
                         <NavLinks to='vantagens' 
                         smooth={true}
                         duration={500}
                         spy={true}
                         exact="true"
                         offset={80}>Veja as vantagens</NavLinks>
                     </NavItem>
                     <NavItem>  
                         <NavLinks to='revenda'
                          smooth={true}
                          duration={500}
                          spy={true}
                          exact="true"
                          offset={80}>Seja um revendedor</NavLinks>
                     </NavItem>
                     <NavBtn>  
                         <NavLinks className="btn-pedir" to='pedir'
                          smooth={true}
                          duration={500}
                          spy={true}
                          exact="true"
                          offset={80}>Peça o seu agora</NavLinks>
                     </NavBtn>
                 </NavMenu>
             </NavbarContainer>
         </Nav>
       </>
    );
};

export default Navbar
