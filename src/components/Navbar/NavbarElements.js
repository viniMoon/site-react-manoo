import styled from 'styled-components';
import { Link as LinkR } from 'react-router-dom';
import { Link as LinkS } from 'react-scroll';
export const Nav = styled.nav `
    background: none;
    height: 80px;
    display:flex;
    justify-content:center;
    align-items:center;
    font-size:1rem;
    position: sticky;
    top:0;
    z-index:10;

    @media screen and (max-width:960px) {
        transition: 0.8s all ease;
    }
`

export const NavbarContainer = styled.div `
    display:flex;
    justify-content:space-between;
    height:80px;
    z-index:1;
    width:100%;
    padding: 0 24px;
    max-width:1100px;
`

export const NavLogo = styled(LinkR) `
    color:#dc3545;
    justify-self:flex-start;
    cursor:pointer;
    font-size:1.5rem;
    display:flex;
    align-items:center;
    margin-left:24px
    font-weight:bold;
    text-decoration:none;

    img {
        width: 50%;
      }
`

export const MobileIcon = styled.div `
    display:none;

    @media screen and (max-width: 768px) {
        display:block;
        position:absolute;
        top:0;
        right:0;
        transform:translate(-100%, 60%);
        font-size:1.8rem;
        cursor:pointer;
        color:#dc3545;
    }
`

export const NavMenu = styled.ul `
    display:flex;
    align-items: center;
    list-style:none;
    text-align:center;

    @media screen and (max-width: 768px){
        display:none;
    }
`
export const NavItem = styled.li `
    height: 80px;
    
    padding:10px;
`

export const NavLinks = styled(LinkS) `
    color: #dc3545;
    border:solid 1px;
    border-radius:5px;
    border-color: #dc3545;
    display:flex;
    align-items:center;
    text-decoration:none;
    padding: 0 1rem;
    height: 100%;
    cursor: pointer;
    width: 177px;

    &.active {
        border-bottom: 3px solid #dc3545;
    }

    &:hover{
        background-color: #dc3545;
        border-color: #dc3545;
        color:#fff;
        transition:all 0.2s ease-in-out
    }

`
export const NavBtn = styled.nav `
    display:flex;
    align-items:center;
    

    @media screen and (max-width: 768px){
        display:none;
    }
`
export const NavBtnLink = styled.nav `
    color: #fff;
    background-color: #dc3545;
    border-color: #dc3545;
    border-radius:6px;
    padding: 23px;
    cursor:pointer;
    width: 177px;

    &:hover{
        box-shadow: 3px 6px 18px 1px #a93c47;
    }

`
