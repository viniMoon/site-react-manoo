import React, {useState, useEffect} from 'react'
import {Grid, TextField,makeStyles,Button} from '@material-ui/core'
import { flexbox } from '@material-ui/system';
import {} from './formElements'


const useStyle = makeStyles(theme => ({
    root: {
        '& .MuiFormControl-root' : {
            width:'80%',
            margin: theme.spacing(1),
            borderRadius: '10px',
            padding: '20px',
            textAlign: 'center',
        },

        '& label.Mui-focused': {
            color: 'white',
          },
          '& .MuiInput-underline:after': {
            borderBottomColor: 'white',
          },  
          '& .MuiOutlinedInput-root': {
            '& fieldset': {
              borderColor: 'white',
            },
        }
    },

    formContent: {
        margin:  theme.spacing(5),
        padding: theme.spacing(3),
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    corpo: {
        background: '#dc3545',
        textAlign: 'center',
        borderRadius: '10px',
    },
    sendbtn: {
        padding: '11px',
        paddingRight: '53px',
        paddingLeft: '53px',
        border: 'solid' ,
        marginBottom: '10px',
    },

   

    inputwhite: {
        color: "white"
      }
}))


const initialFvalues = {
    id: 0,
    name:'',
    email:'',
    mobile:'',
    city:'',
    gender:'male',
    departmentId:'',
    hireDate: new Date(),
    isPermanent: false,
}



export default function Form() {

    const [values, setValues] = useState(initialFvalues);
    const classes = useStyle();

    const handleInputChange = e => {
        const [ name, value ] = e.target
        setValues({
            ...values,
            [name]:value
        })
    }

    return (
        <>
        <forContainer className={classes.formContent} >
            <div className={classes.corpo}>
                <form className={classes.root}>
                    <Grid container>
                        <Grid item xs={12}>
                            <TextField 
                                variante="outlined"
                                label="Nome"
                                value={values.name}
                                inputProps={{ style: { fontFamily: 'nunito', color: 'white'}}}
                            />
                            <TextField
                                variante="outlined"
                                label="E-mail"
                                value={values.email}
                            />
                            <TextField
                                variante="outlined"
                                label="Telefone"
                                value={values.phone}
                            />
                        </Grid>
                    </Grid>
                    <Button variant="outlined" color="#fff" href="#outlined-buttons">
                            Enviar
                    </Button>
                </form>
            </div>
        </forContainer>
        </>
    )   
}

