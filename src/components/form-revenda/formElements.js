import styled from 'styled-components';
import { textField } from 'material-ui';

export const CustomInput = styled(textField)`
   &:focus {
      background-color: white;
   }
`;

export const formContainer = styled.div `
    display:flex;
    justify-content:center;
`
