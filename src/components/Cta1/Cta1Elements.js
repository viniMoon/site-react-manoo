import styled from 'styled-components';

export const CtaContainer = styled.div `
    height: 200px;
    display:flex;
    flex-direction:column;
    justify-content:center;
    align-items:center;
    background: #343a40;
    height: 680px;

    @media screen and (max-width: 767px) {
        position: relative;
        margin-top: 883px;
    }

    `
export const CtaWrapper = styled.div `
        max-width: 1000px;
        margin: 0 auto;
        display:grid;
        grid-template-columns: 3fr 3fr;
        align-items: center;
        grid-gap 16px;
        padding: 0 50px;

        @media screen and (max-width: 767px) {
            grid-template-columns: 1fr;
            padding: 0;
        }

    `
export const CtaBox = styled.div `
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items:center;
    border-radius: 10px;
    max-height: 340px;
    padding: 30px;
    `
export const CtaH1 = styled.h1 `
        font-size: 2.4rem;
        color: #fff;
        font-weight: 100;
    `
export const SaibaBtn = styled.a `
    padding: 22px;
    font-size: 1.4rem;
    padding-left: 105px;
    padding-right: 103px;
    color: #fff;
    background: #dc3545;
    border-radius: 6px;

    @media screen and (max-width: 767px) {
        font-size: 1rem;
    }
    `