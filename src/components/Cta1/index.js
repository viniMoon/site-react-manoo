import React, {useState, useEffect} from 'react'
import {CtaContainer,CtaWrapper,CtaBox,SaibaBtn,CtaH1} from './Cta1Elements'
import {Grid, TextField,makeStyles,Button,OutlinedInput } from '@material-ui/core'

import { flexbox } from '@material-ui/system';


const useStyle = makeStyles(theme => ({
    root: {
        '& .MuiFormControl-root' : {
            width:'80%',
            margin: theme.spacing(1),
            borderRadius: '10px',
            marginTop: '19px',
            textAlign: 'center',
            paddingBottom:'14px',
        },

        '& label.Mui-focused': {
            color: 'white',
            
          },
          '& .MuiInput-underline:after': {
            borderBottomColor: 'white',
          },  
          '& .MuiOutlinedInput-root': {
            '& fieldset': {
              borderColor: '#000',
            },

            '& focus': {
                color:'#fff',
            }
            
        }
    },

    formContent: {
        margin:  theme.spacing(5),
        padding: theme.spacing(3),
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',

    },
    corpo: {
        background: '#dc3545',
        textAlign: 'center',
        borderRadius: '10px',
    },
    sendbtn: {
        padding: '11px',
        paddingRight: '53px',
        paddingLeft: '53px',
        border: 'solid 1px' ,
        marginBottom: '10px',
        
    },

    inputwhite: {
        color: "white"
      },
      cssLabel: {
        "&$cssFocused": {
          color: "#fff !important"
        }
      },
    
      cssOutlinedInput: {
        "&$cssFocused $notchedOutline": {
          borderColor: "#fff !important"
        }
      },
    
      cssFocused: {},
      notchedOutline: {}
}))


const initialFvalues = {
    id: 0,
    
    city:'',
    gender:'male',
    departmentId:'',
    hireDate: new Date(),
    isPermanent: false,
}



export function Cta1(){

    const [values, setValues] = useState(initialFvalues);
    const classes = useStyle();

    const handleInputChange = e => {
        const [ name, value ] = e.target
        setValues({
            ...values,
            [name]:value
        })
    }

    return (
        <CtaContainer>
            <CtaWrapper>
                <CtaBox>
                    <CtaH1>Faça igual <b>a centenas de restaurantes</b> </CtaH1>
                </CtaBox>
                <CtaBox>
                    <SaibaBtn> 
                        Saiba Como
                    </SaibaBtn>
                </CtaBox>
            </CtaWrapper>
            <forContainer className={classes.formContent} >
            <div className={classes.corpo}>
                <form className={classes.root}>
                    <Grid container>
                        <Grid item xs={12}>
                            <TextField 
                                className={classes.cssOutlinedInput}
                                variant="outlined"
                                label="Nome"
                                value={values.name}
                            />
                            <TextField
                                 variant="outlined"
                                 label="Email"
                                 value={values.email}
                            />
                            <TextField
                                id="outlined-basic" 
                                variant="outlined"
                                label="Telefone"
                                value={values.phone}
                            />
                        </Grid>
                    </Grid>
                    <Button className={classes.sendbtn} variant="outlined" color="#fff" href="#outlined-buttons">
                            Enviar
                    </Button>
                </form>
            </div>
        </forContainer>
        </CtaContainer>
    )
}
