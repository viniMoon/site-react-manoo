import React from 'react'

import {InfoContainer,infoWrapper,InfoRow,Column1,TextWrapper,
    TopLine,Heading,Subtitle,BtnWrap,Column2,
    ImgWrap,Img,Button,UlItems} from './Info4Elements'

import Svgico from '../../images/pesquisa.svg'

const InfoSection = (img,alt,buttonLabel) => {
    return (
        <>
            <InfoContainer >
                <infoWrapper>   
                    <InfoRow>
                        <Column1>
                            <TextWrapper>   
                                    <Heading>
                                        <b>Mostre que está Preocupado com a opnião do seu cliente</b>
                                    </Heading>
                                    <UlItems>
                                        <p>É importante saber o que os clientes gostam ou pensam sobre seu produto. Incentive e receba feedback dos seus clientes através de avaliação sobre vários assuntos, tais como: Ambiente, Comida, Atendimento, Segurança e muito mais. Aumente o nível de satisfação do seu estabelecimento. </p> 
                                    </UlItems>
                                    <BtnWrap>
                                        <Button to="home">COMECE AGORA </Button>
                                    </BtnWrap>
                                </TextWrapper>
                        </Column1>
                        <Column2>
                            <ImgWrap>
                                <Img src={Svgico} alt={alt}/>
                            </ImgWrap>
                        </Column2>
                    </InfoRow>
                </infoWrapper>
            </InfoContainer>
        </>
    )   
}

export default InfoSection
