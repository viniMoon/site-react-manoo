import React from 'react'

import {InfoContainer,infoWrapper,InfoRow,Column1,TextWrapper,
    TopLine,Heading,Subtitle,BtnWrap,Column2,
    ImgWrap,Img,Button,UlItems} from './Info3Elements'

import Cellphone from '../../images/cellphone-1.png'

const InfoSection = (img,alt,buttonLabel) => {
    return (
        <>
            <InfoContainer >
                <infoWrapper>   
                    <InfoRow>
                        <Column1>
                            <ImgWrap>
                                <Img src={Cellphone} alt={alt}/>
                            </ImgWrap>
                        </Column1>
                        <Column2>
                        <TextWrapper>   
                                <Heading>
                                      <b>Vale a pena implantar o cardápio digital?</b>     
                                </Heading>
                                <UlItems>
                                    <p>Independente de qual menu digital você escolher, saiba que já é um avanço para o seu estabelecimento. Oferecer essa comodidade aos seus clientes vai fazer com que você saia na frente da concorrência, que provavelmente ainda não conta com esse modelo de pedidos. Com a implantação do cardápio digital, donos de estabelecimentos do setor food service apontam o retorno sobre esse investimento já nos primeiros meses, visto que sua equipe poderá ser reduzida, tendo mais tempo livre para atender melhor os seus clientes. Vale a pena implantar o cardápio digital?</p> 
                                </UlItems>
                                <BtnWrap>
                                    <Button to="home">SOLICITE AGORA </Button>
                                </BtnWrap>
                            </TextWrapper>
                        </Column2>
                    </InfoRow>
                </infoWrapper>
            </InfoContainer>
        </>
    )   
}

export default InfoSection
