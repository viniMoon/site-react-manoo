import React, {useState} from 'react'
import Sidebar from '../components/Sidebar'
import Navbar from '../components/Navbar'
import InfoSection from '../components/infoSection';
import {homeObject} from '../components/infoSection/Data'
import IosSection from '../components/IosSection'
import Services from '../components/Services';
import {Cta1} from '../components/Cta1';
import Info2 from '../components/Info2';
import Info3 from '../components/Info3';
import Info4 from '../components/Info4';
import Cta2Dois from '../components/Cta2Dois';
import Depo from '../components/depo';
import Prices from '../components/prices';
import Garantia from '../components/garantia';
import FooterRed from '../components/footerred';
import Footer from '../components/footer';

const Home = () => {

    const [ isOpen , setIsopen] = useState(false);

    const toggle = () => {
        setIsopen(!isOpen);
    };

    return (
        <>
            <Sidebar isOpen={isOpen} toggle={toggle}/>
            <Navbar toggle={toggle}/>
            <InfoSection   {...homeObject}/>
            <IosSection />
            <Services />
            <Cta1 />
            <Info2  />
            <Info3 />
            <Info4 />
            <Cta2Dois />
            <Depo />
            <Prices />
            <Garantia />
            <FooterRed />
            <Footer />
        </>
    )
}

export default Home
